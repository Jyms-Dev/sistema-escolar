<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Course;
use App\Subject;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('id','DESC')->paginate(5);
        return view('admin.users.index', ['users' => $users]);
    }

    public function create()
    {
        $user = new User;
        $roles = Role::orderBy('name','ASC')->pluck('name','id');
        $courses = Course::orderBy('name','ASC')->pluck('name','id');
        $subjects = Subject::orderBy('name','ASC')->pluck('name','id');
        return view('admin.users.create', ["user" => $user, "roles" => $roles,"courses" => $courses, "subjects" => $subjects]);
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role_id = $request->role_id;
        $user->address = $request->address;
        $user->telephone = $request->telephone;
        $user->type_identification = $request->type_identification;
        $user->identification = $request->identification;
        $user->course_id = $request->course_id;
        $user->subject_id = $request->subject_id;

        if ($user->save()) {
            return redirect('admin/users'); 
        } else {
            return view('admin.users.create', ["user" => $user]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit')->with('user', $user);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->role_id = $request->role_id;
        $user->address = $request->address;
        $user->telephone = $request->telephone;
        $user->type_identification = $request->type_identification;
        $user->identification = $request->identification;
        $user->course_id = $request->course_id;
        $user->subject_id = $request->subject_id;

        if ($user->save()) {
            return redirect('admin/users'); 
        } else {
            return view('admin.users.create', ["user" => $user]);
        }
    }

    public function destroy($id)
    {
        User::destroy($id);
        return redirect('admin/users');
    }
}
