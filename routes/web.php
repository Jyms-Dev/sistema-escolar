<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/main', 'HomeController@home')->name('main');

//RUTAS DEL PANEL DE ADMIN
Route::group(['prefix' => 'admin','middleware' => 'auth'], function(){

    Route::get('/',['as' => 'admin.index', function () {
         return view('admin.index');
        }]);
    //Route::group(['middleware' => 'admin'], function(){

        Route::resource('roles','RoleController');
        Route::get('roles/{id}/destroy', [
            'uses' => 'RoleController@destroy',
            'as' => 'admin.roles.destroy'
        ]);

        Route::resource('users','UserController');
        Route::get('user/{id}/destroy', [
            'uses' => 'UserController@destroy',
            'as' => 'admin.users.destroy'
        ]);

        Route::resource('courses','CourseController');
        Route::get('course/{id}/destroy', [
            'uses' => 'CourseController@destroy',
            'as' => 'admin.courses.destroy'
        ]);

        Route::resource('subjects','SubjectController');
        Route::get('subjcts/{id}/destroy', [
            'uses' => 'SubjectController@destroy',
            'as' => 'admin.subjects.destroy'
        ]);
    //});


});