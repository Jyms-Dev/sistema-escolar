<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->integer('role_id');
            $table->string('address');
            $table->string('telephone');
            $table->integer('type_identification');
            $table->integer('identification');
            $table->integer('course_id');
            $table->integer('subject_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->integer('role_id');
            $table->string('address');
            $table->string('telephone');
            $table->integer('type_identification');
            $table->integer('identification');
            $table->integer('course_id');
            $table->integer('subject_id');
        });
    }
}
