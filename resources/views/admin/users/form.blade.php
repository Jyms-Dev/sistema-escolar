{!! Form::open(['url' => $url, 'method' => $method]) !!}
	
<div class="form-group">
	{!! Form::text('name', $user->name, ['class' => 'form-control','required', 'placeholder' => 'Nombre..']) !!}
</div>
<div class="form-group">
	<select class="form-control" id="type_identification" name="type_identification">
		<option value="1">Tarjeta de identidad</option>
		<option value="2">Cedula de ciudadania</option>
	</select>
</div>
<div class="form-group">
	{!! Form::number('identification', $user->identification, ['class' => 'form-control','required', 'placeholder' => 'Identificacion..']) !!}
</div>
<div class="form-group">
	{!! Form::email('email', $user->email, ['class' => 'form-control','required', 'placeholder' => 'Email..']) !!}
</div>
<div class="form-group">
	{!! Form::password('password', ['class' => 'form-control','required', 'placeholder' => 'password..']) !!}
</div>
<div class="form-group">
	{!! Form::select('role_id',$roles,null,['class' => 'form-control', 'id' => 'role_id', 'required'])!!}
</div>
<div class="form-group">
	{!! Form::text('address', $user->address, ['class' => 'form-control','required', 'placeholder' => 'Direccion..']) !!}
</div>
<div class="form-group">
	{!! Form::number('telephone', $user->telephone, ['class' => 'form-control','required', 'placeholder' => 'Telefono..']) !!}
</div>
<div class="form-group" style="display: none" id="course_div">
	{!! Form::select('course_id',$courses,null,['class' => 'form-control'])!!}
</div>
<div class="form-group" style="display: none" id="subject_div">
	{!! Form::select('subject_id',$subjects,null,['class' => 'form-control'])!!}
</div>
<div class="form-group text-right">
	<a href="{{url('/admin/users')}}"> Regresar al listado de Usuarios</a>
	<input type="submit" value="Enviar" class="btn btn-success">
</div>

{!! Form::close() !!}