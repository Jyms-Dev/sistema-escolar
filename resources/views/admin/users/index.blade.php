@extends('layouts.app')

@section('title','Roles')

@section('content')
  <div class="container">
  	<div class="row">
  		<div class="col-md-8  col-md-offset-2">
  			<div class="panel panel-default">
  				<div class="panel-heading">
  					Usuarios
  					
  				</div>
  				<div class="panel-body">
  					<div class="pull-right">
  						<a href="{{url('/admin/users/create')}}"class="btn btn-primary">
		               	Nuevo
		             	</a>
  					</div>
						<table class="table table-bordered">
							<thead>
								<th>ID</th>
								<th>Nombre</th>
								<th>Email</th>
								<th>Rol</th>
								<th>Accion</th>
							</thead>
							<tbody>
								@foreach($users as $user)
								<tr>
									<td>{{ $user->id }}</td>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>

									@foreach($user->roles as $role)
									<td>{{ $role->name }}</td>
									@endforeach
									
									<td>
										<a href="{{url('/admin/users/'.$user->id.'/edit')}}"class="btn btn-warning">
					                     	Editar
					                   	</a>
					                   	<a href="{{ route('admin.users.destroy', $user->id)}}" onclick="return confirm('¿Seguro que deseas eliminarlo')"class="btn btn-danger">
					                     	Eliminar
					                   	</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="text-center">
			         {!! $users->render() !!}
			     </div>
					</div>
  			</div>
  		</div>
  	</div>
  </div>
@endsection