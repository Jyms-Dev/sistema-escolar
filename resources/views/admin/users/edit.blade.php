@extends("layouts.app")

@section("content")
	<div class="container white">
		<h1>Editar Usuario</h1>
		<!-- Formulario-->
		@include('admin.users.form',['user' => $user, 'url' => '/admin/users/'.$user->id, 'method' => 'PUT'])
	</div>
@endsection