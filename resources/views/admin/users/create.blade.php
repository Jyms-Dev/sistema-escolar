@extends("layouts.app")

@section("content")
	<div class="container white">
		<h1>Crear Usuario</h1>
		<!-- Formulario-->
		@include('admin.users.form',['role' => $user, 'url' => '/admin/users', 'method' => 'POST'])
	</div>
@endsection