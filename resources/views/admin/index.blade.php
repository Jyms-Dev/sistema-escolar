@extends('layouts.app')

@section('title','Inicio')

@section('content')
  <h3 class="text-center">Bienvenido al panel de administrador</h3> 
  <hr>
  <div class="row">
  	<div class="col-md-8 col-md-offset-2">
  		<div class="row">
  			<div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">
                <a href="{{url('/admin/users')}}" class="thumbnail">
                    <img class="img-responsive" src="image/students.jpeg" alt="...">
                </a>
                <a href="{{url('/admin/users')}}"><h5 class="text-center">Estudiantes</h5></a>
                
            </div>
          </div>
	      </div>
	      <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">
                <a href="{{url('/admin/users')}}" class="thumbnail">
                    <img class="img-responsive" src="image/teachers.jpeg" alt="...">
                </a>
                <a href="{{url('/admin/users')}}"><h5 class="text-center">Profesores</h5></a>
                
            </div>
          </div>
	      </div>
	      <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">
                <a href="{{url('/admin/subjects')}}" class="thumbnail">
                    <img class="img-responsive" src="image/subjects.jpeg" alt="...">
                </a>
                <a href="{{url('/admin/subjects')}}"><h5 class="text-center">Materias</h5></a>
                
            </div>
          </div>
	      </div>
	      <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">
                <a href="{{url('/admin/courses')}}" class="thumbnail">
                    <img class="img-responsive" src="image/courses.jpeg" alt="...">
                </a>
                <a href="{{url('/admin/courses')}}"><h5 class="text-center">Cursos</h5></a>
                
            </div>
          </div>
	      </div>
  		</div>
  	</div>
  </div>
@endsection