@extends("layouts.app")

@section("content")
	<div class="container white">
		<h1>Editar Cursos</h1>
		<!-- Formulario-->
		@include('admin.courses.form',['course' => $course, 'url' => '/admin/courses/'.$course->id, 'method' => 'PUT'])
	</div>
@endsection