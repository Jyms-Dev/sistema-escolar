{!! Form::open(['url' => $url, 'method' => $method]) !!}
	
<div class="form-group">
	{!! Form::text('name', $course->name, ['class' => 'form-control', 'placeholder' => 'Nombre..']) !!}
</div>
<div class="form-group text-right">
	<a href="{{url('/admin/courses')}}"> Regresar al listado de Cursos</a>
	<input type="submit" value="Enviar" class="btn btn-success">
</div>

{!! Form::close() !!}