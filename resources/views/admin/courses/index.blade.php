@extends('layouts.app')

@section('title','Cursos')

@section('content')
  <div class="container">
  	<div class="row">
  		<div class="col-md-8  col-md-offset-2">
  			<div class="panel panel-default">
  				<div class="panel-heading">
  					Cursos
  					
  				</div>
  				<div class="panel-body">
  					<div class="pull-right">
  						<a href="{{url('/admin/courses/create')}}"class="btn btn-primary">
               	Nuevo
             	</a>
  					</div>
						<table class="table table-bordered">
							<thead>
								<th>ID</th>
								<th>Nombre</th>
								<th>Accion</th>
							</thead>
							<tbody>
								@foreach($courses as $course)
								<tr>
									<td>{{ $course->id }}</td>
									<td>{{ $course->name }}</td>
									<td>
										<a href="{{url('/admin/courses/'.$course->id.'/edit')}}"class="btn btn-warning">
					                     	Editar
					                   	</a>
					                   	<a href="{{ route('admin.courses.destroy', $course->id)}}" onclick="return confirm('¿Seguro que deseas eliminarlo')"class="btn btn-danger">
					                     	Eliminar
					                   	</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="text-center">
			         {!! $courses->render() !!}
			     </div>
					</div>
  			</div>
  		</div>
  	</div>
  </div>
@endsection