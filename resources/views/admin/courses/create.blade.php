@extends("layouts.app")

@section("content")
	<div class="container white">
		<h1>Crear Curso</h1>
		<!-- Formulario-->
		@include('admin.courses.form',['course' => $course, 'url' => '/admin/courses', 'method' => 'POST'])
	</div>
@endsection