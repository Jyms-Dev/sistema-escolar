@extends("layouts.app")

@section("content")
	<div class="container white">
		<h1>Editar Role</h1>
		<!-- Formulario-->
		@include('admin.subjects.form',['subject' => $subject, 'url' => '/admin/subjects/'.$subject->id, 'method' => 'PUT'])
	</div>
@endsection