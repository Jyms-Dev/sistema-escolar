@extends("layouts.app")

@section("content")
	<div class="container white">
		<h1>Crear Materias</h1>
		<!-- Formulario-->
		@include('admin.subjects.form',['subject' => $subject, 'url' => '/admin/subjects', 'method' => 'POST'])
	</div>
@endsection