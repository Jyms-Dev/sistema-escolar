{!! Form::open(['url' => $url, 'method' => $method]) !!}
	
<div class="form-group">
	{!! Form::text('name', $subject->name, ['class' => 'form-control', 'placeholder' => 'Nombre..']) !!}
</div>
<div class="form-group">
	{!! Form::textarea('description', $subject->description, ['class' => 'form-control', 'placeholder' => 'Descripcion..']) !!}
</div>
<div class="form-group text-right">
	<a href="{{url('/admin/subjects')}}"> Regresar al listado de Materias</a>
	<input type="submit" value="Enviar" class="btn btn-success">
</div>

{!! Form::close() !!}