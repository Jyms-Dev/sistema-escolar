@extends('layouts.app')

@section('title','Materias')

@section('content')
  <div class="container">
  	<div class="row">
  		<div class="col-md-8  col-md-offset-2">
  			<div class="panel panel-default">
  				<div class="panel-heading">
  					Materias
  					
  				</div>
  				<div class="panel-body">
  					<div class="pull-right">
  						<a href="{{url('/admin/subjects/create')}}"class="btn btn-primary">
               	Nuevo
             	</a>
  					</div>
						<table class="table table-bordered">
							<thead>
								<th>ID</th>
								<th>Nombre</th>
								<th>Descripcion</th>
								<th>Accion</th>
							</thead>
							<tbody>
								@foreach($subjects as $subject)
								<tr>
									<td>{{ $subject->id }}</td>
									<td>{{ $subject->name }}</td>
									<td>{{ $subject->description }}</td>
									<td>
										<a href="{{url('/admin/subjects/'.$subject->id.'/edit')}}"class="btn btn-warning">
					                     	Editar
					                   	</a>
					                   	<a href="{{ route('admin.subjects.destroy', $subject->id)}}" onclick="return confirm('¿Seguro que deseas eliminarlo')"class="btn btn-danger">
					                     	Eliminar
					                   	</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="text-center">
			         {!! $subjects->render() !!}
			     </div>
					</div>
  			</div>
  		</div>
  	</div>
  </div>
@endsection