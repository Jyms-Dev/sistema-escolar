@extends('layouts.app')

@section('title','Roles')

@section('content')
  <div class="container">
  	<div class="row">
  		<div class="col-md-8  col-md-offset-2">
  			<div class="panel panel-default">
  				<div class="panel-heading">
  					Roles
  					
  				</div>
  				<div class="panel-body">
  					<div class="pull-right">
  						<a href="{{url('/admin/roles/create')}}"class="btn btn-primary">
               	Nuevo
             	</a>
  					</div>
						<table class="table table-bordered">
							<thead>
								<th>ID</th>
								<th>Nombre</th>
								<th>Descripcion</th>
								<th>Accion</th>
							</thead>
							<tbody>
								@foreach($roles as $role)
								<tr>
									<td>{{ $role->id }}</td>
									<td>{{ $role->name }}</td>
									<td>{{ $role->description }}</td>
									<td>
										<a href="{{url('/admin/roles/'.$role->id.'/edit')}}"class="btn btn-warning">
					                     	Editar
					                   	</a>
					                   	<a href="{{ route('admin.roles.destroy', $role->id)}}" onclick="return confirm('¿Seguro que deseas eliminarlo')"class="btn btn-danger">
					                     	Eliminar
					                   	</a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="text-center">
			         {!! $roles->render() !!}
			     </div>
					</div>
  			</div>
  		</div>
  	</div>
  </div>
@endsection