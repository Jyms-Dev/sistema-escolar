@extends("layouts.app")

@section("content")
	<div class="container white">
		<h1>Editar Role</h1>
		<!-- Formulario-->
		@include('admin.roles.form',['role' => $role, 'url' => '/admin/roles/'.$role->id, 'method' => 'PUT'])
	</div>
@endsection