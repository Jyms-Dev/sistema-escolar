{!! Form::open(['url' => $url, 'method' => $method]) !!}
	
<div class="form-group">
	{!! Form::text('name', $role->name, ['class' => 'form-control', 'placeholder' => 'Nombre..']) !!}
</div>
<div class="form-group">
	{!! Form::textarea('description', $role->description, ['class' => 'form-control', 'placeholder' => 'Descripcion..']) !!}
</div>
<div class="form-group text-right">
	<a href="{{url('/admin/roles')}}"> Regresar al listado de Roles</a>
	<input type="submit" value="Enviar" class="btn btn-success">
</div>

{!! Form::close() !!}