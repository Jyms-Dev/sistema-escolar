@extends("layouts.app")

@section("content")
	<div class="container white">
		<h1>Crear Role</h1>
		<!-- Formulario-->
		@include('admin.roles.form',['role' => $role, 'url' => '/admin/roles', 'method' => 'POST'])
	</div>
@endsection