$(document).ready(function(){
	$('#role_id').change(function(){
		if ($('#role_id').val() == 3) {
			$('#course_div').show();
		} else {
			$('#course_div').hide();
		}

		if ($('#role_id').val() == 2) {
			$('#subject_div').show();
		} else {
			$('#subject_div').hide();
		}
	})
});
